import React from 'react'

import './icon.css'

const Icon = () => 
<div className="icon">
<span className="drawic">&#x2764;</span>
<span className="drawic">&#x2709;</span>
<span className="drawic">&#x260e;</span>
<span className="drawic">&#x2746;</span>
<span className="drawic">&#x266b;</span>
</div>


export default Icon