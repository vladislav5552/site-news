import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './partnerNewsListItem.css'


class PartnerNewsListItem  extends Component {

    static propTypes = {
        description: PropTypes.string,
        
    }
    render() {
        
        const {
         description,
        } = this.props
    return (
        <div className="partner">
            <div className="partner-description">{description}</div>
        </div>


    )
}
    }
 
    export default PartnerNewsListItem