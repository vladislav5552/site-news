import React from 'react'

import PartnerNewsListItem from './PartnerNewsListItem'
import partnerNews from './partnerNews'

const PartnerNewsList = () => {
    return (
        <div className="products-list">
            <div className="row">
            {
                partnerNews.map(({
                    id,
                    description,
                    image,
                })=>(
                        <div className="news-partner "  key={id}>
                            <PartnerNewsListItem
                                description={description}
                                image={image}
                            />
                        </div>
                    ))
                }

            </div>
        </div>
    )
}

export default PartnerNewsList
