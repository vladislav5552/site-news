import React from 'react'
import {Link} from 'react-router-dom'
import './mainNewsListItem.css'

 
 const MainNewsListItem = () => {

        return (
            <div className="main-news">
            <div className="main-photo"><img src='/images/photo1.png' alt="" /></div>
            <div className="main-description"> <Link to="/news">«Наш пострел»: Потомок киевлян возглавил Иллинойс </Link></div>
            <div className="main-time">12:36 | Новости</div>
       </div>
        )
    }
export default MainNewsListItem