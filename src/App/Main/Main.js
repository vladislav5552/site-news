import React from 'react'
import {Route} from 'react-router-dom'
import MainNewsList from './MainNews/MainNewsList'
import ActualyNewsList from './ActualyNews/ActualyNewsList'
import './main.css'
import PartnerNewsList from './PartnerNews/PartnerNewsList';
import SmiNewsList from './SmiNews/SmiNewsList';
import  NewsPage from './News/NewsPage';
// import CartPage from './Cart/CartPage';


const Main = () => 

<main className="main">
   <div className="container">
         <div className="row">
                 <Route path='/' exact render={() => {
                          return ( 
                                <div className="col-md-8">   
                   
                                <div className="title">Новости</div>
                                <div className="main-news">
                                
                                     <MainNewsList/>
                                    
                                </div>
                                <div className="row">
                                     <ActualyNewsList/>
                                     
                                </div>
                                <div className="submit">Загрузить еще</div>
                                </div>
               )	
}}/>
<Route path="/news" render = {() => {
        return ( 
        <NewsPage/>
        )
 }}/>
 
                 <div className="col-md-4"> 
                         <div className="row">
                         <div className="title-partner">Самое читаемое</div>
                                  <PartnerNewsList/>
                                  <hr className='hr'/>
                         </div>
                </div>
                <div  className="col-md-12">
                      <hr/>
                      <div className="title">Новости партнеров</div>
                      <SmiNewsList/>
                </div>
         </div> 
   </div>    
</main>

export default Main

