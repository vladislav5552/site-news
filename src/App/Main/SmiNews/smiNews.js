const smiNews = [
    { 
     id:1,
     description:"Скончалась актриса сериалов 'Моя прекрасная няня' и 'Интерны'",
     image :"/images/smi1.jpeg" 
    },
    {   
     id:2,
     description:"Последние слова Хашогги: «Я задыхаюсь, снимите мешок с моей головы»",
     image :"/images/smi2.jpeg" 
    },

    {   
     id:3,
     description:"В Европу без Украины. «Газпром» готовится к худшему?",
     image :"/images/smi3.jpeg" 
    },
 
    {   
     id:4,
     description:"Россия лишь объявила о строительстве парома, как Литва снизила тариф на 10%",
     image :"/images/smi4.jpeg" 
    },

    {   
     id:5,
     description:"Чехия разгромила Россию на «Кубке Карьяла»",
     image :"/images/smi5.jpeg" 
    },
    
   
 ]
 
export default smiNews