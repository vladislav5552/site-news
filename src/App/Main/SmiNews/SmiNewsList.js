import React from 'react'

import SmiNewsListItem from './SmiNewsListItem'
import smiNews from './smiNews'

const SmiNewsList = () => {
    return (
        
            <div className="row">
            {
                smiNews.map(({
                    id,
                    description,
                    image,
                })=>(
                        <div  key={id}>
                            <SmiNewsListItem
                                description={description}
                                image={image}
                                
                            />
                        </div>
                    ))
                }

            </div>
        
    )
}

export default SmiNewsList