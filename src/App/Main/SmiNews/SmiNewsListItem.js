import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './smiNewsListItem.css'


class SmiNewsListItem  extends Component {

    static propTypes = {
        description: PropTypes.string,
  
    }
    render() {
        
        const {
         description,
         image,
        } = this.props
    return (
        <div className="smi">
             <div className ="col-md-2">
               <div className="smi-image"><img src={image} alt={description}/></div>
               <div className="smi-description">{description}</div>
             </div>
        </div>
    )
}
    }
 
    export default SmiNewsListItem




   