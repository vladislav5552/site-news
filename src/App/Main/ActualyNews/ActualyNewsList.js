import React from 'react'

import ActualyNewsListItem from './ActualyNewsListItem'
import actualyNews from './actualyNews'

const ActualyNewsList = () => {
    return (
        
            <div className="row">
            {
                actualyNews.map(({
                    id,
                    description,
                    time,
                    image,
                })=>(
                        <div  key={id}>
                            <ActualyNewsListItem
                                description={description}
                                time={time}
                                image={image}
                                
                            />
                        </div>
                    ))
                }

            </div>
        
    )
}

export default ActualyNewsList
