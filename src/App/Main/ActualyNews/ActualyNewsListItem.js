import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types'
import './actualyNewsListItem.css'


class ActualyNewsListItem  extends Component {

    static propTypes = {
        description: PropTypes.string,
        time:PropTypes.number,
        
    }
    render() {
        
        const {
         description,
         time,
         image,
        } = this.props
    return (
        <div className="actualy-news">
            <div className="news-description"><Link to="/news">{description}</Link> <br></br>
            <span className="actialy-time">{time}</span></div>
            <div className="photo-news"> <img src={image} alt={description}/></div>
        </div>
    )
}
    }
 
    export default ActualyNewsListItem

